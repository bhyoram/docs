You can install the gitlab client from the gitlab-cli package in debian.
```
apt install gitlab-cli
```

# Moving a repository
```
gitlab project transfer-project --id --id debian-pm%2F${project} --to-namespace ${namespace}
```

OR 

```
gitlab group transfer-project --id debian-pm%2F${project} --to-project-id debian-pm%2F${group}%2F${project}
```

# Creating a subgroup
```
gitlab group create --name Apps \
	--path apps \
	--parent-id 2972697 \ ## group id of debian-pm
	--description "mobile friendly applications" \
	--visibility public
```

# Get id of a group
```
gitlab --fields id -o yaml group get --id debian-pm
```
