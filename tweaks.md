# Tweaks to start working with debian-pm

Assuming you have the packages installed, you will need some manual commands to make plasma-mobile run instead of the current graphical
environment. You will also want to disable ModemManager if you want to run ofono (phone stack used by plasma-mobile):
`systemctl disable phosh`
`systemctl disable ModemManager`
`systemctl enable ofono`
`systemctl enable simplelogin`
Now you can reboot to plamsa-mobile

## Bypass lockscreen
Plasma mobile lockscreen was changed in 5.21 from numeric keypads that support numeric password only, to full text password field.
However, you may experience a bug that eliminates the on-screen-keyboard from lockscreen. If that happens, you can pass it from
command-line:
`loginctl unlock-session c1`

## Settings application
Plasma mobile has application called plasma-settings to manage, settings... Some settings, however (Icons size for example) are only accessible in the KDE systemsettings application:
`apt install systemsettings`
Note that this application has very poor usability on phone environment, so it's only usefull for settings that are not in 
plasma-settings. you will probably want to enlarge (panel and small) icons and to increase screen brightness. you can also use this 
applications to disable lock-screen.  

## More usefull packages
Make sure that kaccounts-integration is installed. Also, if you deal with ofono issues, you may want to install ofonoctl.
