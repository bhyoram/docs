# Installation

First, download a generic rootfs image from [here](https://archive.kaidan.im/debian-pm/images/).
You will also need a second part, specific to your device.
You can either build such an image yourself using the halium buildsystem,
or download a prebuilt one from [the server](https://archive.kaidan.im/halium/) if one is available for your device.
The image you need in this step is called system.img.

To install both images to your device,
you can use this [halium-install](https://github.com/JBBgameich/halium-install/releases) script.

Reboot your device into the TWRP recovery, and use
`halium-install -p debian-pm rootfs.tar.xz system.img` to install the images.
Please note that you need to use `halium-install -p debian-pm-caf rootfs.tar.xz system.img` to install the images on caf (Qualcomm) devices.

The last step is flashing the boot.img, which is also a halium part.
Depending on the device, you can use fastboot or heimdal to do it.
